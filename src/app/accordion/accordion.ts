import {animate, animation, style} from '@angular/animations';

export const Accordion = animation([
  style({
    height: '{{heightStart}}',
  }),
  animate(
    '{{ time }}',
    style({
      height: '{{heightEnd}}',
    })),
]);
