import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {transition, trigger, useAnimation} from '@angular/animations';
import {Accordion} from './accordion';

interface Accordion {
  title: string;
  description: string;
  id: number;
}

interface Log {
  closed: string;
  checked: string;
}

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  animations: [
    trigger('accordion', [

      transition(':enter', [
        useAnimation(Accordion, {
          params: {
            heightStart: 0,
            heightEnd: '*',
            time: '.1s'
          }
        })
      ]),

      transition(':leave', [
        useAnimation(Accordion, {
          params: {
            heightStart: '*',
            heightEnd: 0,
            time: '.1s'
          }
        })
      ]),
    ]),
  ]
})

export class AccordionComponent implements OnInit {

  @Input() accordion: Accordion[];
  @Output() checked = new EventEmitter<any>();

  selected: number;
  log: Log = {
    closed: '',
    checked: ''
  };

  constructor() {
  }

  ngOnInit() {
    this.addAccordionId();
  }

  addAccordionId() {
    this.accordion.forEach((cur, index) => {
      this.accordion[index].id = index;
    });
  }

  show(checked: number) {
    this.loging(checked);
    this.selected = checked;
  }

  isCurrent(checked: number) {
    if (this.selected === checked) {
      return true;
    }
  }

  loging(checked) {
    if (this.selected === checked) {
      return;
    }
    this.selected !== undefined ? this.log.closed = this.accordion[this.selected].title : this.log.closed = 'none';
    checked !== undefined ? this.log.checked = this.accordion[checked].title : this.log.checked = 'none';
    this.checked.emit(this.log);
  }
}
