import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  accordion = [
    {
      title: 'TITLE-1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    },
    {
      title: 'TITLE-2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    },
    {
      title: 'TITLE-3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    },
    {
      title: 'TITLE-4',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    },
    {
      title: 'TITLE-5',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    },
    {
      title: 'TITLE-6',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
        'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' +
        'Donec ac odio tempor orci dapibus ultrices in iaculis nunc. ' +
        'Turpis in eu mi bibendum neque.'
    }
  ];

  log(event) {
    console.log(event);
  }
}
